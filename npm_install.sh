#!/bin/bash
(diff package.json ~/custom_cache/package_latest.json) 1>/dev/null
need_update=$?

if [ $need_update -eq 1 ]
then
    (npm install)
    success=$?

    if [ $success -eq 0 ]
    then
        echo -e "Removing module cache...\n"
        rm -rf ~/custom_cache/node_modules
        rm -f ~/custom_cache/package_latest.json
        echo -e "Saving new module cache...\n"
        cp -r node_modules ~/custom_cache/node_modules
        cp package.json ~/custom_cache/package_latest.json
        echo -e "Sucessfully updated module cache\n"
    else
        echo -e "Failure\n"
        exit 1
    fi

elif [ $need_update -eq 0 ]
then
    echo -e "No new dependency found, npm install has been ignored\n"
    echo -e "Modules cache is being used...\n"
    cp -r ~/custom_cache/node_modules ./
    echo -e "Cache has been setup for this pipeline successfully.\n"
fi
